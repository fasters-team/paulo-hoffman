import Vue from 'vue'
import App from './App'
import Router from 'vue-router' // routes
import VueHead from 'vue-head' // manipulating head meta tags
import VueResource from 'vue-resource' // http client
import VuePaginate from 'vue-paginate' // paginação
import * as VueGoogleMaps from 'vue2-google-maps'
// config
Vue.config.productionTip = false

// use
Vue.use(Router)
Vue.use(VueResource)
Vue.use(VueHead, { separator: '|', complement: 'Empresa' })
Vue.use(VuePaginate)
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAHJrcik09jEzMrtWMWwBu3wVgpxn-rYg0',
   }})

// pages
import Home from '@/pages/Home'
import QuemSomos from '@/pages/quem-somos'
import Atuacao from '@/pages/Atuacao'
import Empresarial from '@/pages/AtuaEmp'
import Civil from '@/pages/AtuaCivil'
import Consumidor from '@/pages/AtuaConsumidor'
import Imobiliario from '@/pages/AtuaImob'
import Familia from '@/pages/AtuaFamilia'
import Saude from '@/pages/AtuaSaude'
import Divorcio from '@/pages/AtuaDivorcio'
import Trabalho from '@/pages/AtuaTrabalho'
import Recuperacao from '@/pages/AtuaRecupe'
import Contato from '@/pages/Contato'
import Trabalhe from '@/pages/Trabalhe'
import Midia from '@/pages/Midia'
import publicacao from '@/pages/publicacao'


//filter
Vue.filter('truncate', function (text, stop, clamp) {
  return text
    .replace(/&nbsp;/gi,'') // remove &nbsp
    .replace(/<(?:.|\n)*?>/gm, '') // remove tags
    .replace('/\r?\n|\r/g', ' ') // remove breaklines
    .slice(0, stop) + (stop < text.length ? clamp || '...' : '')
})

// router
const router = new Router({
  routes: [
    { path: '/QuemSomos', component: QuemSomos},
    { path: '/Atuacao', component: Atuacao},
    { path: '/Atuacao/Empresarial', component: Empresarial},
    { path: '/Atuacao/Civil', component: Civil},
    { path: '/Atuacao/Consumidor', component: Consumidor},
    { path: '/Atuacao/Imobiliario', component: Imobiliario},
    { path: '/Atuacao/Familia', component: Familia},
    { path: '/Atuacao/Trabalho', component: Trabalho},
    { path: '/Atuacao/Divorcio', component: Divorcio},
    { path: '/Atuacao/Saude', component: Saude},
    { path: '/Atuacao/Recuperacao', component: Recuperacao},
    { path: '/Contato', component: Contato},
    { path: '/Trabalhe', component: Trabalhe},
    { path: '/Midia', component: Midia},
    { path: '/publicacao', component: publicacao},
    { path: '/', component: Home},
  ],
  mode: 'history',
  scrollBehavior ( to ) {
    if (!to.params.tabId ) {
      $('html, body').animate({ scrollTop:0 }, { duration: "slow" });
    }
  $('#navbarNavDropdown').collapse('hide');
  return false;
}
})

// vue instance
new Vue({
  el: '#app',
  template: '<App/>',
  components: { App },
  router
})


